package com.example.examplemod.tile_entities;

import com.example.examplemod.Constants.Globals;
import com.example.examplemod.ExampleMod;
import cpw.mods.fml.common.registry.GameRegistry;

/**
 * Created by mclark4386 on 9/30/14.
 */
public class TileEntityTest {
    public static void mainRegistry(){
        registerTileEntities();
    }

    private static void registerTileEntities() {
        GameRegistry.registerTileEntity(TileEntityTestChest.class, Globals.MODID);
    }
}
