package com.example.examplemod.client.gui;

import com.example.examplemod.Constants.Globals;
import com.example.examplemod.handlers.ConfigHandler;
import cpw.mods.fml.client.config.GuiConfig;
import net.minecraft.client.gui.GuiScreen;
import net.minecraftforge.common.config.ConfigElement;
import net.minecraftforge.common.config.Configuration;

/**
 * Created by mclark4386 on 10/7/14.
 */
public class ModGuiConfig extends GuiConfig{
    public ModGuiConfig(GuiScreen guiScreen){
       super(guiScreen,
               new ConfigElement(ConfigHandler.configuration.getCategory(Configuration.CATEGORY_GENERAL)).getChildElements(),
               Globals.MODID,
               false,
               true,
               GuiConfig.getAbridgedConfigPath(ConfigHandler.configuration.toString())
       );
    }
}
