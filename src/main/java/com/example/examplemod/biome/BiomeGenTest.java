package com.example.examplemod.biome;

import com.example.examplemod.blocksanditems.BlocksAndItems;
import com.example.examplemod.blocksanditems.ModBlocks;
import net.minecraft.entity.monster.EntitySlime;
import net.minecraft.init.Blocks;
import net.minecraft.world.biome.BiomeGenBase;

/**
 * Created by mclark4386 on 9/30/14.
 */
public class BiomeGenTest extends BiomeGenBase{
    public BiomeGenTest(int id) {
        super(id);

        spawnableCreatureList.add(new SpawnListEntry(EntitySlime.class,5,2,10));

        theBiomeDecorator.treesPerChunk = 5;
        theBiomeDecorator.grassPerChunk = 5;
        theBiomeDecorator.bigMushroomsPerChunk = 3;


        topBlock = Blocks.grass;
        fillerBlock = ModBlocks.testBlock;
    }
}
