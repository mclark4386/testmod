package com.example.examplemod.biome;

import net.minecraft.world.biome.BiomeGenBase;
import net.minecraftforge.common.BiomeDictionary;
import net.minecraftforge.common.BiomeManager;

/**
 * Created by mclark4386 on 9/30/14.
 */
public class BiomeRegistry {
    public static void mainRegistry(){
        initBiome();
        registerBiome();
    }

    public static BiomeGenBase biomeTest;

    private static void initBiome() {
        biomeTest = new BiomeGenTest(137).setBiomeName("TestBiome").setHeight(new BiomeGenBase.Height(4.0F, 0.025F)).setTemperatureRainfall(1.2f,0.9f);
    }

    private static void registerBiome() {
        BiomeDictionary.registerBiomeType(biomeTest, BiomeDictionary.Type.DENSE);
        BiomeManager.addSpawnBiome(biomeTest);
    }
}
