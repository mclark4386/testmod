package com.example.examplemod.Constants;

/**
 * Created by mclark4386 on 10/7/14.
 */
public class LoadedConfigs {
    public static boolean enableTestArmor = true;
    public static boolean enableTestFood = true;
    public static boolean enableTestTool = true;

    public static boolean enableNightVisionGoggles = true;
    public static boolean enableRebreather = true;
}
