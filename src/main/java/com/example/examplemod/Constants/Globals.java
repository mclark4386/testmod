package com.example.examplemod.Constants;

/**
 * Created by mclark4386 on 10/3/14.
 */
public class Globals {
    public static final String MODID = "examplemod";
    public static final String MOD_NAME = "Test Mod";
    public static final String VERSION = "1.7.10-0.02";
    public static final String CLIENTPROXY = "com.example.examplemod.proxy.ClientProxy";
    public static final String SERVERPROXY = "com.example.examplemod.proxy.ServerProxy";
    public static final String GUI_FACTORY_CLASS = "com.example.examplemod.client.gui.GUIFactory";

    public static final String RESOURCE_PREFIX = MODID.toLowerCase()+":";
}
