package com.example.examplemod.enchantments;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnumEnchantmentType;

/**
 * Created by mclark4386 on 9/30/14.
 */
public class EnchantmentSpeedBoost extends Enchantment {
    public EnchantmentSpeedBoost(int id, int rarity){
        super(id,rarity, EnumEnchantmentType.armor_feet);
        this.setName("speedBoost");
    }

    /**
     * Returns the minimal value of enchantability needed on the enchantment level passed.
     */
    public int getMinEnchantability(int i)
    {
        return 5 + (i-1) * 10;
    }

    /**
     * Returns the maximum value of enchantability nedded on the enchantment level passed.
     */
    public int getMaxEnchantability(int i)
    {
        return this.getMinEnchantability(i) + 20;
    }

    /**
     * Returns the maximum level that the enchantment can have.
     */
    public int getMaxLevel()
    {
        return 3;
    }


}
