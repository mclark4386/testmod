package com.example.examplemod;

import com.example.examplemod.blocksanditems.BlocksAndItems;
import com.example.examplemod.blocksanditems.ModItems;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.PlayerEvent;
import net.minecraft.item.ItemStack;

/**
 * Created by mclark4386 on 9/30/14.
 */
public class TestOnMineEvent {
    @SubscribeEvent
    public void whenIGetAnIngot(PlayerEvent.ItemPickupEvent e){
        if (e.pickedUp.getEntityItem().isItemEqual(new ItemStack(ModItems.testIngot))){
            e.player.addStat(ExampleMod.achievementIngot,1);
        }
    }
}
