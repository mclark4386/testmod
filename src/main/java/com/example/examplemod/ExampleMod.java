package com.example.examplemod;

import com.example.examplemod.Constants.Globals;
import com.example.examplemod.Utils.LogHelper;
import com.example.examplemod.biome.BiomeRegistry;
import com.example.examplemod.biome.WorldTypeTest;
import com.example.examplemod.blocksanditems.BlocksAndItems;
import com.example.examplemod.blocksanditems.ModItems;
import com.example.examplemod.handlers.ConfigHandler;
import com.example.examplemod.enchantments.EnchantmentSpeedBoost;
import com.example.examplemod.handlers.Recipes;
import com.example.examplemod.proxy.IProxy;
import com.example.examplemod.tile_entities.TileEntityTest;
import com.example.examplemod.world.TestWorld;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.item.ItemStack;
import net.minecraft.stats.Achievement;
import net.minecraft.world.WorldType;
import net.minecraftforge.common.AchievementPage;

@Mod(modid = Globals.MODID, version = Globals.VERSION, guiFactory = Globals.GUI_FACTORY_CLASS)
public class ExampleMod
{
    @Mod.Instance(Globals.MODID)
    public static ExampleMod instance;

    @SidedProxy(clientSide = Globals.CLIENTPROXY, serverSide = Globals.SERVERPROXY)
    public static IProxy proxy;

    public static Achievement achievementIngot;
    public static Achievement achievementTool;

    public static final Enchantment speedBoost = new EnchantmentSpeedBoost(84,5);

    @EventHandler
    public void preInit(FMLPreInitializationEvent event){
        //read/setup config
        ConfigHandler.init(event.getSuggestedConfigurationFile());
        FMLCommonHandler.instance().bus().register(new ConfigHandler());

        //do our thing
        BlocksAndItems.initAndRegister();
        Recipes.init();
        TestWorld.mainRegistry();
        TileEntityTest.mainRegistry();
        BiomeRegistry.mainRegistry();

        proxy.registerRenderThings();
        LogHelper.info("pre-init complete!");
    }
    
    @EventHandler
    public void init(FMLInitializationEvent event)
    {
        achievementIngot = new Achievement("achievement.mineIngot","mineIngot",0,0,new ItemStack(ModItems.testIngot), (Achievement)null).initIndependentStat().registerStat();//put AchievementList.acquireIron instead of null to make this and dependent on the vanilla chain
        achievementTool = new Achievement("achievement.craftTool","craftTool",2,1,new ItemStack(ModItems.item1),achievementIngot).registerStat();

        AchievementPage.registerAchievementPage(new AchievementPage("Test Mod Achievements",new Achievement[]{achievementIngot,achievementTool}));

        FMLCommonHandler.instance().bus().register(new TestOnMineEvent());
        FMLCommonHandler.instance().bus().register(new ToolOnCraftEvent());

        LogHelper.info("init complete!");
    }

    @EventHandler
    public void postInit(FMLPostInitializationEvent event) {
        WorldType TEST = new WorldTypeTest(3,"test");
        LogHelper.info("post-init complete!");
    }
}
