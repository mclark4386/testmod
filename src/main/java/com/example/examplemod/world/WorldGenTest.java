package com.example.examplemod.world;

import com.example.examplemod.ExampleMod;
import com.example.examplemod.blocksanditems.BlocksAndItems;
import com.example.examplemod.blocksanditems.ModBlocks;
import cpw.mods.fml.common.IWorldGenerator;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.feature.WorldGenMinable;

import java.util.Random;

/**
 * Created by mclark4386 on 9/29/14.
 */
public class WorldGenTest implements IWorldGenerator {

    @Override
    public void generate(Random random, int chunkX, int chunkZ, World world, IChunkProvider chunkGenerator, IChunkProvider chunkProvider) {
        switch (world.provider.dimensionId){
            case -1:
                generateNether(world,random,chunkX*16,chunkZ*16);
                break;
            case 0:
                generateSurface(world, random, chunkX * 16, chunkZ * 16);
                break;
        }
    }

    private void generateSurface(World world, Random random, int chunkX, int chunkZ) {
        for (int i = 0; i < 100; i++) {//limit is the number of veins per chunk
            int randPosX = chunkX + random.nextInt(16);
            int randPosY = random.nextInt(64);
            int randPosZ = chunkZ + random.nextInt(16);

            (new WorldGenMinable(ModBlocks.testBlock,10)).generate(world,random,randPosX,randPosY,randPosZ);
        }
    }

    private void generateNether(World world, Random random, int chunkX, int chunkZ) {
        //Nada atm...
    }
}
