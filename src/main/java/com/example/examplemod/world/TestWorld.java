package com.example.examplemod.world;

import cpw.mods.fml.common.IWorldGenerator;
import cpw.mods.fml.common.registry.GameRegistry;

/**
 * Created by mclark4386 on 9/29/14.
 */
public class TestWorld {
    public static void mainRegistry(){
        initWorldGen();
    }

    private static void initWorldGen() {
        registerWorldGen(new WorldGenTest(),1);
    }

    private static void registerWorldGen(IWorldGenerator worldGenClass, int weightedProbablity){
        GameRegistry.registerWorldGenerator(worldGenClass,weightedProbablity);
    }
}
