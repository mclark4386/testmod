package com.example.examplemod.proxy;

/**
 * Created by mclark4386 on 10/3/14.
 */
public interface IProxy {
    public abstract void registerRenderThings();
}
