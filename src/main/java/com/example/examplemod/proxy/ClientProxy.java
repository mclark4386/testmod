package com.example.examplemod.proxy;

import com.example.examplemod.ServerTickHandler;
import com.example.examplemod.blocksanditems.BlocksAndItems;
import com.example.examplemod.blocksanditems.ModBlocks;
import com.example.examplemod.render.TestChestRenderer;
import com.example.examplemod.render.items.ItemRendererTestChest;
import com.example.examplemod.tile_entities.TileEntityTestChest;
import cpw.mods.fml.client.registry.ClientRegistry;
import cpw.mods.fml.common.FMLCommonHandler;
import net.minecraft.client.Minecraft;
import net.minecraft.item.Item;
import net.minecraftforge.client.MinecraftForgeClient;

/**
 * Created by mclark4386 on 9/30/14.
 */
public class ClientProxy extends CommonProxy {
    public void registerRenderThings(){
        ClientRegistry.bindTileEntitySpecialRenderer(TileEntityTestChest.class,new TestChestRenderer());
        MinecraftForgeClient.registerItemRenderer(Item.getItemFromBlock(ModBlocks.testChest),new ItemRendererTestChest());
        FMLCommonHandler.instance().bus().register(new ServerTickHandler(Minecraft.getMinecraft()));
    }
}
