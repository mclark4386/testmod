package com.example.examplemod.render;

import com.example.examplemod.blocksanditems.TestChest;
import com.example.examplemod.tile_entities.TileEntityTestChest;
import cpw.mods.fml.common.FMLLog;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.client.model.ModelChest;
import net.minecraft.client.model.ModelLargeChest;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

import java.util.Calendar;

/**
 * Created by mclark4386 on 9/29/14.
 */
@SideOnly(Side.CLIENT)
public class TestChestRenderer extends TileEntitySpecialRenderer
{
    private static final ResourceLocation trappedLargeTexture = new ResourceLocation("textures/entity/chest/trapped_double.png");
    private static final ResourceLocation xmasLargeTexture = new ResourceLocation("textures/entity/chest/christmas_double.png");
    private static final ResourceLocation LargeTexture = new ResourceLocation("textures/entity/chest/normal_double.png");
    private static final ResourceLocation trappedTexture = new ResourceLocation("textures/entity/chest/trapped.png");
    private static final ResourceLocation xmasTexture = new ResourceLocation("textures/entity/chest/christmas.png");
    private static final ResourceLocation texture = new ResourceLocation("textures/entity/chest/normal.png");
    private ModelChest modelChest = new ModelChest();
    private ModelChest modelLargeChest = new ModelLargeChest();
    private boolean isXmas;
    private static final String __OBFID = "CL_00000965";

    public TestChestRenderer()
    {
        Calendar calendar = Calendar.getInstance();

        if (calendar.get(2) + 1 == 12 && calendar.get(5) >= 24 && calendar.get(5) <= 26)
        {
            this.isXmas = true;
        }
    }

    public void renderTileEntityAt(TileEntityTestChest tileEntityChest, double x, double y, double z, float tick)
    {
        int i;

        if (!tileEntityChest.hasWorldObj())
        {
            i = 0;
        }
        else
        {
            Block block = tileEntityChest.getBlockType();
            i = tileEntityChest.getBlockMetadata();

            if (block instanceof TestChest && i == 0)
            {
                try
                {
                    ((TestChest)block).func_149954_e(tileEntityChest.getWorldObj(), tileEntityChest.xCoord, tileEntityChest.yCoord, tileEntityChest.zCoord);
                }
                catch (ClassCastException e)
                {
                    FMLLog.severe("Attempted to render a chest at %d,  %d, %d that was not a chest", tileEntityChest.xCoord, tileEntityChest.yCoord, tileEntityChest.zCoord);
                }
                i = tileEntityChest.getBlockMetadata();
            }

            tileEntityChest.checkForAdjacentChests();
        }

        if (tileEntityChest.adjacentChestZNeg == null && tileEntityChest.adjacentChestXNeg == null)
        {
            ModelChest modelchest;

            if (tileEntityChest.adjacentChestXPos == null && tileEntityChest.adjacentChestZPos == null)
            {
                modelchest = this.modelChest;

                if (tileEntityChest.func_145980_j() == 1)
                {
                    this.bindTexture(trappedTexture);
                }
                else if (this.isXmas)
                {
                    this.bindTexture(xmasTexture);
                }
                else
                {
                    this.bindTexture(texture);
                }
            }
            else
            {
                modelchest = this.modelLargeChest;

                if (tileEntityChest.func_145980_j() == 1)
                {
                    this.bindTexture(trappedLargeTexture);
                }
                else if (this.isXmas)
                {
                    this.bindTexture(xmasLargeTexture);
                }
                else
                {
                    this.bindTexture(LargeTexture);
                }
            }

            GL11.glPushMatrix();
            GL11.glEnable(GL12.GL_RESCALE_NORMAL);
            GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
            GL11.glTranslatef((float)x, (float)y + 1.0F, (float)z + 1.0F);
            GL11.glScalef(1.0F, -1.0F, -1.0F);
            GL11.glTranslatef(0.5F, 0.5F, 0.5F);
            short short1 = 0;

            if (i == 2)
            {
                short1 = 180;
            }

            if (i == 3)
            {
                short1 = 0;
            }

            if (i == 4)
            {
                short1 = 90;
            }

            if (i == 5)
            {
                short1 = -90;
            }

            if (i == 2 && tileEntityChest.adjacentChestXPos != null)
            {
                GL11.glTranslatef(1.0F, 0.0F, 0.0F);
            }

            if (i == 5 && tileEntityChest.adjacentChestZPos != null)
            {
                GL11.glTranslatef(0.0F, 0.0F, -1.0F);
            }

            GL11.glRotatef((float)short1, 0.0F, 1.0F, 0.0F);
            GL11.glTranslatef(-0.5F, -0.5F, -0.5F);
            float f1 = tileEntityChest.prevLidAngle + (tileEntityChest.lidAngle - tileEntityChest.prevLidAngle) * tick;
            float f2;

            if (tileEntityChest.adjacentChestZNeg != null)
            {
                f2 = tileEntityChest.adjacentChestZNeg.prevLidAngle + (tileEntityChest.adjacentChestZNeg.lidAngle - tileEntityChest.adjacentChestZNeg.prevLidAngle) * tick;

                if (f2 > f1)
                {
                    f1 = f2;
                }
            }

            if (tileEntityChest.adjacentChestXNeg != null)
            {
                f2 = tileEntityChest.adjacentChestXNeg.prevLidAngle + (tileEntityChest.adjacentChestXNeg.lidAngle - tileEntityChest.adjacentChestXNeg.prevLidAngle) * tick;

                if (f2 > f1)
                {
                    f1 = f2;
                }
            }

            f1 = 1.0F - f1;
            f1 = 1.0F - f1 * f1 * f1;
            modelchest.chestLid.rotateAngleX = -(f1 * (float)Math.PI / 2.0F);
            modelchest.renderAll();
            GL11.glDisable(GL12.GL_RESCALE_NORMAL);
            GL11.glPopMatrix();
            GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        }
    }

    public void renderTileEntityAt(TileEntity tileEntity, double x, double y, double z, float tick)
    {
        this.renderTileEntityAt((TileEntityTestChest)tileEntity, x, y, z, tick);
    }
}