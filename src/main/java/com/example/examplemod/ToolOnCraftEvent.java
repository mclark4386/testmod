package com.example.examplemod;

import com.example.examplemod.blocksanditems.BlocksAndItems;
import com.example.examplemod.blocksanditems.ModItems;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.PlayerEvent;
import net.minecraft.item.ItemStack;

/**
 * Created by mclark4386 on 9/30/14.
 */
public class ToolOnCraftEvent {
    @SubscribeEvent
    public void whenIGetAnIngot(PlayerEvent.ItemCraftedEvent e){
        if (e.crafting.getItem().equals(ModItems.item1)){
            e.player.addStat(ExampleMod.achievementTool,1);
        }
    }
}
