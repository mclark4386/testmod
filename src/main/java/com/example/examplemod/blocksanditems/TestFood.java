package com.example.examplemod.blocksanditems;

import com.example.examplemod.Constants.Globals;
import com.example.examplemod.ExampleMod;
import net.minecraft.item.ItemFood;

/**
 * Created by mclark4386 on 9/28/14.
 */
public class TestFood extends ItemFood {
    public TestFood(int p_i45339_1_, float p_i45339_2_, boolean p_i45339_3_) {
        super(p_i45339_1_, p_i45339_2_, p_i45339_3_);
        setCreativeTab(BlocksAndItems.testTab);
        setTextureName(Globals.MODID+":testFood");
    }
}
