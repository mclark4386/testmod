package com.example.examplemod.blocksanditems;

import com.example.examplemod.Constants.Globals;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

/**
 * Created by mclark4386 on 10/9/14.
 */
@GameRegistry.ObjectHolder(Globals.MODID)
public class ModItems {
    public static Item item1 = new TestItem(ModMaterials.testToolMaterial, 50);
    public static Item testIngot = new TestIngot().setUnlocalizedName("testIngot");

    public static Item helmetTest = new TestArmor(ModMaterials.testArmorMaterial, 5, 0).setUnlocalizedName("helmetTest");
    public static Item chestTest = new TestArmor(ModMaterials.testArmorMaterial, 5, 1).setUnlocalizedName("chestTest");
    public static Item legsTest = new TestArmor(ModMaterials.testArmorMaterial, 5, 2).setUnlocalizedName("legsTest");
    public static Item bootsTest = new TestArmor(ModMaterials.testArmorMaterial, 5, 3).setUnlocalizedName("bootsTest");

    public static Item foodTest = new TestFood(32, 10, false).setUnlocalizedName("testFood");

    //Utilities
    public static Item nightVisionGoggles = new NightVisionGoggles(ModMaterials.testArmorMaterial,5,0).setUnlocalizedName("nightVisionGoggles");
    public static Item rebreather = new Rebreather(ModMaterials.testArmorMaterial,5,1).setUnlocalizedName("rebreather");

   public static void init(){}

    public static void register(){
        GameRegistry.registerItem(item1, "testItem");
        GameRegistry.registerItem(testIngot, "testIngot");
        GameRegistry.registerItem(helmetTest,"helmetTest");
        GameRegistry.registerItem(chestTest,"chestTest");
        GameRegistry.registerItem(legsTest,"legsTest");
        GameRegistry.registerItem(bootsTest,"bootsTest");
        GameRegistry.registerItem(foodTest, "testFood");
        GameRegistry.registerItem(nightVisionGoggles,"nightVisionGoggles");
        GameRegistry.registerItem(rebreather,"rebreather");
    }
}
