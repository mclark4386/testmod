package com.example.examplemod.blocksanditems;

import com.example.examplemod.Constants.Globals;
import com.example.examplemod.ExampleMod;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

import java.util.Random;

/**
 * Created by mclark4386 on 9/27/14.
 */
public class TestBlock extends BlockTM {

    public TestBlock( Material mat){
        super(mat);
        setLightLevel(1.0f);
        setHarvestLevel("pickaxe", 2);
    }

    @Override
    public Item getItemDropped(int par1, Random rand,int par2){
        return ModItems.testIngot;
    }

}
