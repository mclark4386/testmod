package com.example.examplemod.blocksanditems;

import net.minecraft.item.ItemArmor;

/**
 * Created by mclark4386 on 10/8/14.
 */
public class ItemArmorTM extends ItemArmor {
    public ItemArmorTM(ArmorMaterial armorMaterial, int renderIndex, int armorType) {
        super(armorMaterial, renderIndex, armorType);
        setCreativeTab(BlocksAndItems.testTab);
    }
}
