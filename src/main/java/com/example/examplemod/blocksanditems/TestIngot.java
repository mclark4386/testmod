package com.example.examplemod.blocksanditems;

import com.example.examplemod.ExampleMod;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

/**
 * Created by mclark4386 on 9/28/14.
 */
public class TestIngot extends ItemTM {
    public TestIngot(){
        super();
        setMaxStackSize(64);
    }
}
