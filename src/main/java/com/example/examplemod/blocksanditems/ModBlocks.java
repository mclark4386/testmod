package com.example.examplemod.blocksanditems;

import com.example.examplemod.Constants.Globals;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

/**
 * Created by mclark4386 on 10/9/14.
 */
@GameRegistry.ObjectHolder(Globals.MODID)
public class ModBlocks {
    public static Block testBlock  = new TestBlock( Material.rock).setHardness(1.5f).setBlockName("testBlock");
    public static Block testChest = new TestChest(0).setBlockName("testChest");

    public static void init(){}

    public static void register(){
        GameRegistry.registerBlock(testBlock, "testBlock");
        GameRegistry.registerBlock(testChest, "testChest");
    }
}
