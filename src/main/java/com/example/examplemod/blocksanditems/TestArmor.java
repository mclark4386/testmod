package com.example.examplemod.blocksanditems;

import com.example.examplemod.Constants.Globals;
import com.example.examplemod.ExampleMod;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;


/**
 * Created by mclark4386 on 9/28/14.
 */
public class TestArmor extends ItemArmor {
    private String[] armorTypes = new String[] {"helmetTest","chestTest","legsTest","bootsTest"};
    public TestArmor(ArmorMaterial armorMaterial, int renderIndex, int armorType) {
        super(armorMaterial, renderIndex, armorType);
        setCreativeTab(BlocksAndItems.testTab);

    }

    @Override
    public String getArmorTexture(ItemStack itemStack, Entity entity, int slot, String layer){
        if (itemStack.getItem().equals(ModItems.helmetTest)||itemStack.getItem().equals(ModItems.chestTest)||itemStack.getItem().equals(ModItems.bootsTest)) {
            return Globals.MODID+":textures/armor/test_1.png";
        }
        if (itemStack.getItem().equals(ModItems.legsTest)){
            return Globals.MODID+":textures/armor/test_2.png";
        }else{
            return null;
        }
    }

    @Override
    public void registerIcons(IIconRegister iconRegister){
        if (this == ModItems.helmetTest){
            itemIcon = iconRegister.registerIcon(Globals.MODID+":helmetTest") ;
        }
        if (this == ModItems.chestTest){
            itemIcon = iconRegister.registerIcon(Globals.MODID+":chestTest") ;
        }
        if (this == ModItems.legsTest){
            itemIcon = iconRegister.registerIcon(Globals.MODID+":legsTest") ;
        }
        if (this == ModItems.bootsTest){
            itemIcon = iconRegister.registerIcon(Globals.MODID+":bootsTest") ;
        }
    }

    @Override
    public void onArmorTick(World world, EntityPlayer player,ItemStack itemStack){
        if (itemStack.getItem() == ModItems.bootsTest){
            int j = EnchantmentHelper.getEnchantmentLevel(ExampleMod.speedBoost.effectId,itemStack);  //check for enchant
            if (j > 0){
                player.addPotionEffect(new PotionEffect(Potion.moveSpeed.getId(),50,j-1));
            }
        }
    }
}
