package com.example.examplemod.blocksanditems;

import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor;
import net.minecraftforge.common.util.EnumHelper;

/**
 * Created by mclark4386 on 10/9/14.
 */
public class ModMaterials {
    public static Item.ToolMaterial testToolMaterial = EnumHelper.addToolMaterial("TEST", 4, 5000, 20.0f, 25.0f, 25);
    public static ItemArmor.ArmorMaterial testArmorMaterial = EnumHelper.addArmorMaterial("TEST",100,new int[]{5,12,10,5},25);
}
