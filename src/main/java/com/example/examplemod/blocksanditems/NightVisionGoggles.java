package com.example.examplemod.blocksanditems;

import com.example.examplemod.Constants.Globals;
import com.example.examplemod.ExampleMod;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;

/**
 * Created by mclark4386 on 10/3/14.
 */
public class NightVisionGoggles extends ItemArmor {
    private String[] armorTypes = new String[] {"nightVisionGoggles"};
    public NightVisionGoggles(ArmorMaterial armorMaterial, int renderIndex, int armorType) {
        super(armorMaterial, renderIndex, armorType);
        setCreativeTab(BlocksAndItems.utilsTab);
    }

    @Override
    public String getArmorTexture(ItemStack itemStack, Entity entity, int slot, String layer){
        if (itemStack.getItem().equals(ModItems.nightVisionGoggles)) {
            return Globals.MODID+":textures/armor/utilities.png";
        }else{
            return null;
        }
    }

    @Override
    public void registerIcons(IIconRegister iconRegister){
        if (this == ModItems.nightVisionGoggles){
            itemIcon = iconRegister.registerIcon(Globals.MODID+":nightVisionGoggles") ;
        }
    }

    @Override
    public void onArmorTick(World world, EntityPlayer player,ItemStack itemStack){
        if (itemStack.getItem() == ModItems.nightVisionGoggles){
                player.addPotionEffect(new PotionEffect(Potion.nightVision.getId(),50,10));
        }
    }
}
