package com.example.examplemod.blocksanditems;

import com.example.examplemod.Constants.Globals;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

/**
 * Created by mclark4386 on 10/8/14.
 */
public class ItemTM extends Item {
    public ItemTM(){
        super();
        setCreativeTab(BlocksAndItems.testTab);
        setMaxStackSize(1);
    }

    @Override
    public String getUnlocalizedName(){
        return String.format("item.%s%s", Globals.RESOURCE_PREFIX,getUnwrappedUnlocalizedName(super.getUnlocalizedName()));
    }

    @Override
    public String getUnlocalizedName(ItemStack itemStack){
        return String.format("item.%s%s", Globals.RESOURCE_PREFIX,getUnwrappedUnlocalizedName(super.getUnlocalizedName()));
    }

    protected String getUnwrappedUnlocalizedName(String unlocalizedName){
        return unlocalizedName.substring(unlocalizedName.indexOf(".") + 1);
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void registerIcons(net.minecraft.client.renderer.texture.IIconRegister iconRegister) {
        itemIcon = iconRegister.registerIcon(this.getUnwrappedUnlocalizedName(this.getUnlocalizedName()));
    }
}
