package com.example.examplemod.blocksanditems;

import com.example.examplemod.Constants.Globals;
import com.example.examplemod.Constants.LoadedConfigs;
import com.example.examplemod.ExampleMod;
import com.example.examplemod.handlers.Recipes;
import cpw.mods.fml.client.registry.RenderingRegistry;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.util.EnumHelper;

/**
 * Created by mclark4386 on 9/29/14.
 */

public class BlocksAndItems {


    public static final int testBlockId = 500;
    public static final int testIngotId = 501;
    public static final int testItemId = 502;


    public static final int helmetTestId = 503;
    public static final int chestTestId = 504;
    public static final int legsTestId = 505;
    public static final int bootsTestId = 506;

    //Utilities
    public static final int nightVisionGogglesId = 507;
    public static final int rebreatherId = 508;


    public static CreativeTabs testTab = new CreativeTabs("test") {
        @Override
        public Item getTabIconItem() {
            return ModItems.item1;
        }
    };


    public static CreativeTabs utilsTab = new CreativeTabs("Utilities") {
        @Override
        public Item getTabIconItem() {
            return ModItems.nightVisionGoggles;
        }
    };

    public static void initAndRegister(){
        init();
        register();
    }

    private static void init() {
        RenderingRegistry.addNewArmourRendererPrefix("5");

        ModItems.init();
        ModBlocks.init();
    }

    private static void register() {
        ModItems.register();
        ModBlocks.register();
    }
}
