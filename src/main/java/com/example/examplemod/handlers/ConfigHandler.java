package com.example.examplemod.handlers;

import com.example.examplemod.Constants.Globals;
import com.example.examplemod.Constants.LoadedConfigs;
import cpw.mods.fml.client.event.ConfigChangedEvent;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.common.config.Configuration;

import java.io.File;

/**
 * Created by mclark4386 on 10/7/14.
 */
public class ConfigHandler  {
    public static Configuration configuration;

    public static void init(File configFile){
        if (configuration == null) {
            configuration = new Configuration(configFile);
        }

        loadConfig();
    }

    private static void loadConfig() {
        //Test stuff
        LoadedConfigs.enableTestArmor = configuration.get(Configuration.CATEGORY_GENERAL,"enableTestArmor",true,"This toggles if the Test Armor is craftable").getBoolean(true);
        LoadedConfigs.enableTestFood = configuration.get(Configuration.CATEGORY_GENERAL,"enableTestFood",true,"This toggles if the Test Food is craftable").getBoolean(true);
        LoadedConfigs.enableTestTool = configuration.get(Configuration.CATEGORY_GENERAL,"enableTestTool",true,"This toggles if the Test Tool is craftable").getBoolean(true);

        //Utilities
        LoadedConfigs.enableNightVisionGoggles = configuration.get(Configuration.CATEGORY_GENERAL,"enableNightVisionGoggles",true,"This toggles if the Night Vision Goggles are craftable").getBoolean(true);
        LoadedConfigs.enableRebreather = configuration.get(Configuration.CATEGORY_GENERAL,"enableRebreather",true,"This toggles if the Rebreather is craftable").getBoolean(true);

        if (configuration.hasChanged()) {
            configuration.save();
        }
    }

    @SubscribeEvent
    public void onConfigurationChangedEvent(ConfigChangedEvent.OnConfigChangedEvent event){
        if (event.modID.equalsIgnoreCase(Globals.MODID)){
            //Resync Configs
            loadConfig();
        }
    }
}
