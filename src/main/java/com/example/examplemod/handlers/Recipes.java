package com.example.examplemod.handlers;

import com.example.examplemod.Constants.LoadedConfigs;
import com.example.examplemod.ExampleMod;
import com.example.examplemod.blocksanditems.BlocksAndItems;
import com.example.examplemod.blocksanditems.ModBlocks;
import com.example.examplemod.blocksanditems.ModItems;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;

/**
 * Created by mclark4386 on 10/8/14.
 */
public class Recipes {
    public static void init(){

        ItemStack swordIS = new ItemStack(ModItems.item1);
        ItemStack helmIS = new ItemStack(ModItems.helmetTest);
        ItemStack chestIS = new ItemStack(ModItems.chestTest);
        ItemStack legsIS = new ItemStack(ModItems.legsTest);
        ItemStack bootsIS = new ItemStack(ModItems.bootsTest);
        ItemStack ingotIS = new ItemStack(ModItems.testIngot);
        ItemStack stickIS = new ItemStack(Items.stick);
        ItemStack diamondIS = new ItemStack(Items.diamond);
        ItemStack chestBlockIS = new ItemStack(ModBlocks.testChest);

        if(LoadedConfigs.enableTestTool) {
            GameRegistry.addRecipe(swordIS, "x", "x", "y", 'x', ingotIS, 'y', stickIS);
        }


        if(LoadedConfigs.enableTestArmor) {
            GameRegistry.addRecipe(helmIS, "xxx", "x x", 'x', ingotIS);
            GameRegistry.addRecipe(chestIS, "x x", "xxx", "xxx", 'x', ingotIS);
            GameRegistry.addRecipe(legsIS, "xxx", "x x", "x x", 'x', ingotIS);
            GameRegistry.addRecipe(bootsIS, "x x", "x x", 'x', ingotIS);
        }

        GameRegistry.addRecipe(chestBlockIS,"xxx","x x","xxx", 'x',ingotIS);
//        GameRegistry.registerTileEntity(TileEntityTestChest,"Test Chest");
        GameRegistry.addSmelting(ModBlocks.testBlock,ingotIS,10);


        if(LoadedConfigs.enableTestFood) {
            GameRegistry.addSmelting(ingotIS, new ItemStack(ModItems.foodTest), 10);
        }

        ItemStack enchantedBootsIS = new ItemStack(ModItems.bootsTest);
        enchantedBootsIS.addEnchantment(ExampleMod.speedBoost,3);
        enchantedBootsIS.addEnchantment(Enchantment.featherFalling,10);

        if(LoadedConfigs.enableTestArmor) {
            GameRegistry.addRecipe(bootsIS, "x x", "xdx", 'x', ingotIS, 'd', diamondIS);
        }


        //Utilities
        ItemStack nightVisionGogglesIS = new ItemStack(ModItems.nightVisionGoggles);
        ItemStack rebreatherIS = new ItemStack(ModItems.rebreather);
        ItemStack leatherIS = new ItemStack(Items.leather);
        ItemStack glowstoneIS = new ItemStack(Blocks.glowstone);
        ItemStack ironIS = new ItemStack(Items.iron_ingot);
        ItemStack leavesIS = new ItemStack(Blocks.leaves);
        ItemStack leaves2IS = new ItemStack(Blocks.leaves2);


        GameRegistry.addRecipe(nightVisionGogglesIS,"lil","l l","gig",'l',leatherIS,'i',ironIS,'g',glowstoneIS);

        GameRegistry.addRecipe(rebreatherIS,"lil","igi","lil",'l',leatherIS,'i',ironIS,'g',leavesIS);
        GameRegistry.addRecipe(rebreatherIS,"lil","igi","lil",'l',leatherIS,'i',ironIS,'g',leaves2IS);
    }
}
